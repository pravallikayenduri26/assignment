package com.wipro.scientificcalculator;

public class TrigCalc {
	public double getSinValue(int degree) {
        return Math.sin(Math.toRadians(degree));
    }

    public static void main(String[] args) {
        TrigCalc trigCalc = new TrigCalc();
        System.out.println("sin(0) = " + trigCalc.getSinValue(0));   
        System.out.println("sin(30) = " + trigCalc.getSinValue(30));
        System.out.println("sin(45) = " + trigCalc.getSinValue(45)); 
        System.out.println("sin(60) = " + trigCalc.getSinValue(60)); 
        System.out.println("sin(90) = " + trigCalc.getSinValue(90)); 
    }

}
