package com.wipro.scientificcalculator;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class CalculatorTest {
	@Test
    public void testSin0() {
        TrigCalc trigCalc = new TrigCalc();
        assertEquals(0.0, trigCalc.getSinValue(0));
    }

    @Test
    public void testSin30() {
        TrigCalc trigCalc = new TrigCalc();
        assertEquals(0.49999999999999994, trigCalc.getSinValue(30));
    }

    @Test
    public void testSin45() {
        TrigCalc trigCalc = new TrigCalc();
        assertEquals(0.7071067811865475, trigCalc.getSinValue(45));
    }

    @Test
    public void testSin60() {
        TrigCalc trigCalc = new TrigCalc();
        assertEquals(0.8660254037844386, trigCalc.getSinValue(60));
    }

    @Test
    public void testSin90() {
        TrigCalc trigCalc = new TrigCalc();
        assertEquals(1.0, trigCalc.getSinValue(90));
    }

}
